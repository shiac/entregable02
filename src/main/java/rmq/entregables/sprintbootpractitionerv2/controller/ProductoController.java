package rmq.entregables.sprintbootpractitionerv2.controller;

import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.web.bind.annotation.*;
import rmq.entregables.sprintbootpractitionerv2.model.ProductoModel;
import rmq.entregables.sprintbootpractitionerv2.services.ProductoService;

import java.util.List; import java.util.Optional;

@RestController
@RequestMapping("${url.base}")
public class ProductoController {
    @Autowired
    ProductoService productoService;

    @GetMapping("/productos")
    public List<ProductoModel> getProductos() {
        return productoService.findAll();
    }

    @GetMapping("/productos/{id}" )
    public Optional<ProductoModel> getProductoId(@PathVariable String id){
        return productoService.findById(id);
    }

    @PostMapping("/productos")
    public ProductoModel postProductos(@RequestBody ProductoModel newProducto){
        productoService.save(newProducto);
        return newProducto;
    }

    @PutMapping("/productos/{id}")
    public void putProductos(@RequestBody ProductoModel productoToUpdate,@PathVariable String id){
        productoService.reemplazar(id,productoToUpdate);
    }

    @DeleteMapping("/productos")
    public boolean deleteProductos(@RequestBody ProductoModel productoToDelete){
        return productoService.delete(productoToDelete);
    }
}