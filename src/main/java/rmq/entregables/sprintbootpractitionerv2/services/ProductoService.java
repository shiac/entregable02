package rmq.entregables.sprintbootpractitionerv2.services;


import rmq.entregables.sprintbootpractitionerv2.model.ProductoModel;

import java.util.List;
import java.util.Optional;

public interface ProductoService {

    public List<ProductoModel> findAll();

    public Optional<ProductoModel> findById(String  id) ;

    public ProductoModel save(ProductoModel productoModel) ;

    public boolean delete(ProductoModel productoModel) ;

    public void reemplazar(String id, ProductoModel productoModel);
}