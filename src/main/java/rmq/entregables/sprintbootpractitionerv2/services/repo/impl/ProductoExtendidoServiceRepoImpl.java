package rmq.entregables.sprintbootpractitionerv2.services.repo.impl;

import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.data.mongodb.core.MongoTemplate;
import org.springframework.data.mongodb.core.query.Criteria;
import org.springframework.data.mongodb.core.query.Query;
import org.springframework.data.mongodb.core.query.Update;
import rmq.entregables.sprintbootpractitionerv2.model.ProductoModel;
import rmq.entregables.sprintbootpractitionerv2.services.repo.ProductoExtendidoServiceRepo;


public class ProductoExtendidoServiceRepoImpl implements ProductoExtendidoServiceRepo {

    @Autowired
    MongoTemplate mongoTemplate;

     public void modificarProducto(String id, ProductoModel productoModel){
       final Query filtro=new Query();
       filtro.addCriteria(Criteria.where("_id").is(id));

       final Update update =new Update();
         setearCampoNotNull(update,"descripcion",productoModel.getDescripcion());
         setearCampoNotZero(update,"precio",productoModel.getPrecio());
        this.mongoTemplate.updateFirst(filtro,update,productoModel.getClass());
     }

     private void setearCampoNotNull(Update update, String nombre,String valor){
         if(valor!=null && !valor.trim().isEmpty()){
             update.set(nombre,valor.trim());
         }
     }

    private void setearCampoNotZero(Update update, String nombre,Double valor){
        if(valor!=null && valor!=0){
            update.set(nombre,valor);
        }
    }


}
