package rmq.entregables.sprintbootpractitionerv2.services.repo;

import org.springframework.data.mongodb.repository.MongoRepository;
import org.springframework.stereotype.Repository;
import rmq.entregables.sprintbootpractitionerv2.model.ProductoModel;


@Repository
public interface ProductoExtendidoServiceRepo  {

    public void modificarProducto(String id, ProductoModel productoModel);
}
