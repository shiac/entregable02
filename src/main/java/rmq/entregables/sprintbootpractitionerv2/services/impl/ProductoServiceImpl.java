package rmq.entregables.sprintbootpractitionerv2.services.impl;

import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Service;
import rmq.entregables.sprintbootpractitionerv2.model.ProductoModel;
import rmq.entregables.sprintbootpractitionerv2.services.ProductoService;
import rmq.entregables.sprintbootpractitionerv2.services.repo.ProductoServiceRepo;

import java.util.List;
import java.util.Optional;

@Service
public class ProductoServiceImpl  implements ProductoService {

    @Autowired
    ProductoServiceRepo productoServiceRepo;

    public List<ProductoModel> findAll() {
        return productoServiceRepo.findAll();
    }

    public Optional<ProductoModel> findById(String  id) {
        return productoServiceRepo.findById(id);
    }

    public ProductoModel save(ProductoModel productoModel) {
        return productoServiceRepo.save(productoModel);
    }

    public boolean delete(ProductoModel productoModel) {
        try {
            productoServiceRepo.delete(productoModel);
            return true;
        } catch(Exception ex) {
            return false;
        }
    }

    public void reemplazar(String id, ProductoModel productoModel) {
        productoServiceRepo.modificarProducto( id,  productoModel);
    }
}

