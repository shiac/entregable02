package rmq.entregables.sprintbootpractitionerv2;

import org.springframework.boot.SpringApplication;
import org.springframework.boot.autoconfigure.SpringBootApplication;

@SpringBootApplication
public class SprintBootPractitionerv2Application {

	public static void main(String[] args) {
		SpringApplication.run(SprintBootPractitionerv2Application.class, args);
	}

}
